<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

Route::get('/', function () {
    if (!Auth::check()){
        return redirect('/login');
    }
    return redirect('/home');
});
Auth::routes();


Route::get('login', function () {
    if (!Auth::check()) {
        return view('game-net/login');
    }
    else {
        return redirect('/');
    }
});

Route::get('logout', function () {
    if (Auth::check()) {
        Auth::logout();
        return redirect('/');
    }
    else {
        return redirect('/');
    }
});

Route::get('register', 'GameNetController@register_form');

Route::get('test', function () {
    return view('game-net.register');
});

Route::get('home', 'GameNetController@controller');

Route::get('game-net/paid-orders', 'GameNetController@paid_orders_page');

Route::get('game-net/change-settings', 'GameNetController@change_request');

Route::get('game-net/add-order', 'GameNetController@add_order_form');

Route::post('game-net/operations/add-order', 'GameNetController@add_order');

Route::get('game-net/finish-order/{id}', 'GameNetController@finish_game');

Route::get('game-net/orders', 'GameNetController@orders_page');

Route::get('admin-panel', 'AdminController@dashboard_page');
