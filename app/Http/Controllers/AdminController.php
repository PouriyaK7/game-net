<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    function dashboard_page() {
        if (Auth::check()) {
            if (Auth::user()->access > 5) {
                return view('admin-panel.dashbord');
            }
        }
        return redirect('/');
    }

    function add_game(Request $request) {
        if (Auth::check()) {
            if (Auth::user()->access > 5) {
                $inputs = $request->except('_token');
                DB::table('games')
                    ->insert([
                        'title' => $inputs['title'],
                        'price' => $inputs['price'],
                        'release_year' => $inputs['release-year'],
                        'platform' => $inputs['platform'],
                        'rate' => $inputs['rate']
                    ]);
                return redirect('/admin-panel/games');
            }
        }
        return redirect('/');
    }

    function add_game_net(Request $request) {
        if (Auth::check()) {
            if (Auth::user()->access > 5) {
                $inputs = $request->except('_token');
                DB::table('game-nets')
                    ->insert([
                        'title' => $inputs['title'],
                        'address' => $inputs['address'],
                        'location' => $inputs['location'],
                        'consoles' => $inputs['consoles'],
                        'office_number' => $inputs['office-number'],
                        'phone_number' => $inputs['phone-number'],
                        'description' => $inputs['description']
                    ]);
                return redirect('/admin-panel/game-nets');
            }
        }

        return redirect('/');
    }

    function add_consoles_to_game_nets(Request $request) {
        if (Auth::check()) {
            if (Auth::user()->access > 5) {
                $inputs = $request->except('_token');
                $x = DB::table('gem_net_consoles')
                    ->insertGetId([
                        'game_net' => $inputs['game-net'],
                        'type' => $inputs['type'],
                        'code' => $inputs['code']
                    ]);

                DB::table('game_net_prices')
                    ->insert([
                        'game_net' => $inputs['game-net'],
                        'console' => $x,
                        'price' => $inputs['price']
                    ]);
                return redirect('/admin-panel/consoles');
            }
        }

        return redirect('/');
    }

    function add_news(Request $request) {
        if (Auth::check()) {
            if (Auth::user()->access > 5) {
                $inputs = $request->except('_token');
                $tags = explode("\n", $inputs['tags']);
                $tags = json_encode($tags);
                DB::table('news')
                    ->insert([
                        'title' => $inputs['title'],
                        'author' => Auth::id(),
                        'text' => $inputs['text'],
                        'tags' => $tags,
                    ]);
                if ($request->hasFile('img')) {

                }
            }
        }
    }
}
