<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GameNetController extends Controller
{
    public function controller() {
        if (Auth::check()) {
            $current_date = Carbon::now();
            if (Auth::user()->game_net != null && Auth::user()->end_date > $current_date) {
                return redirect('/game-net/orders');
            }
            elseif (Auth::user()->access > 5) {
                return redirect('/admin-panel');
            }
            elseif (Auth::user()->game_net != null && Auth::user()->end_date < $current_date) {
                Auth::logout();
                return redirect('/login?error=activation-is-passed');
            }
            else return '<h1>404 Not Found</h1>';
        }
        else {
            return redirect('/');
        }
    }

    public function home_page() {
        if (Auth::check()) {
            if (Auth::user()->game_net != null) {
                $x = DB::table('orders')
                    ->where('game_net', '=', Auth::user()->game_net)
                    ->where('status', '=', 1)
                    ->paginate(30);
                return view('home', compact('x'));
            }
        }
        return redirect('/');
    }

   public function orders_page() {
        if (Auth::check()) {
            $current_date = Carbon::now();
            if (Auth::user()->game_net != null && Auth::user()->end_date > $current_date) {
                $x = DB::table('orders')
                    ->select([
                        '*',
                        'orders.id AS order_id',
                        'orders.price AS order_price',
                        'orders.description AS order_description',
                        'orders.date AS order_date'
                    ])
                    ->join('gem_net_consoles', function ($join) {
                        $join->on('gem_net_consoles.id', '=', 'orders.console');
                    })
                    ->join('game_net_prices', function ($join) {
                        $join->on('game_net_prices.console', '=', 'orders.console');
                    })
                    ->where('orders.game_net', '=', Auth::user()->game_net)
                    ->where('status', '=', 1)
                    ->paginate(30);


                return view('game-net.orders', compact('x'));
            }
        }

        return redirect('/');
   }

    public function paid_orders_page() {
        if (Auth::check()) {
            $current_date = Carbon::now();
            if (Auth::user()->game_net != null && $current_date < Auth::user()->end_date) {
                $x = DB::table('orders')
                    ->select([
                        '*',
                        'orders.id AS order_id',
                        'orders.price AS order_price',
                        'orders.description AS order_description',
                        'orders.date AS order_date',
                    ])
                    ->join('gem_net_consoles', function ($join) {
                        $join->on('gem_net_consoles.id', '=', 'orders.console');
                    })
                    ->join('game_net_prices', function ($join) {
                        $join->on('game_net_prices.console', '=', 'orders.console');
                    })
                    ->where('orders.game_net', '=', Auth::user()->game_net)
                    ->where('status', '=', 0)
                    ->orderBy('orders.id', 'DESC')
                    ->paginate(30);
                $sum = DB::table('orders')
                    ->where('status', '=', 0)
                    ->sum('price');
                return view('game-net.paid-orders', compact('x', 'sum'));
            }
        }
        return redirect('/');
    }

    public function accounting_page() {
        // TODO have to talk with mmd about this
    }

    public function change_request() {
        if (Auth::check()) {
            $current_date = Carbon::now();
            if (Auth::user()->game_net != null && Auth::user()->request == 0 && Auth::user()->end_date > $current_date) {
                DB::table('requests')
                    ->insert([
                        'game_net' => Auth::user()->game_net
                    ]);
                DB::table('users')
                    ->where('id', '=', Auth::id())
                    ->update(['request' => 1]);
                return redirect('/home');
            }
        }
        return redirect('/');
    }

    public function add_order(Request $request) {
        if (Auth::check()) {
            $current_date = Carbon::now();
            $inputs = $request->except('_token');
            if (Auth::user()->game_net != null && Auth::user()->end_date > $current_date && $inputs['gamepad-count'] > 0) {
                $x = DB::table('game_net_prices')
                    ->where('console', '=', $inputs['console'])
                    ->where('game_net', '=', Auth::user()->game_net)
                    ->where('gamepad_count', '=', $inputs['gamepad-count'])
                    ->get();
                if (count($x) == 1) {
                    $price = $x[0]->price * $inputs['time'];
                    DB::table('orders')
                        ->insert([
                            'console' => $inputs['console'],
                            'game_net' => Auth::user()->game_net,
                            'price' => $price,
                            'time' => $inputs['time'],
                            'description' => $inputs['description'],
                            'gamepad_count' => $inputs['gamepad-count']
                        ]);
                    return redirect('/game-net/orders');
                }
            }
            elseif ($inputs['gamepad-count'] == 0) {
                return redirect('/game-net/orders?error=1');
            }
        }
        return redirect('/');
    }

    public function register_form() {
        if (Auth::check()) {
            if (Auth::user()->type > 5) {
                return view('game-net.register');
            }
        }
        return redirect('/');
    }

    public function add_order_form() {
        if ( Auth::check()) {
            $current_date = Carbon::now();
            if ( Auth::user()->game_net != null && Auth::user()->end_date > $current_date) {
                $x = DB::table('gem_net_consoles')
                    ->where('game_net', '=', Auth::user()->game_net)
                    ->get([
                        '*'
                    ]);
                return view('game-net.add-order', compact('x'));
            }
        }

        return redirect('/');
    }

    public function finish_game($id) {
        if (Auth::check()) {
            $current_date = Carbon::now();
            if (Auth::user()->game_net != null && Auth::user()->end_date > $current_date) {
                DB::table('orders')
                    ->where('id', '=', $id)
                    ->update([
                        'status' => 0
                    ]);
                return redirect('/game-net/orders');
            }
        }


        return redirect('/');
    }

}
