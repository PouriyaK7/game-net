<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{
    function home_page() {
        $new_game_nets = DB::table('game-nets')
            ->orderBy('id', 'DESC')
            ->take(5)
            ->get();

        $promoted_game_nets = DB::table('game-nets')
            ->where('promote', '=', 1)
            ->take(5)
            ->get();

        $new_games = DB::table('games')
            ->orderBy('id', 'DESC')
            ->get();

        return view('', compact('new_game_nets', 'new_games', 'promoted_game_nets'));
    }

    function game_net_page($id) {
        $game_net = DB::table('game-nets')
            ->where('game-nets.id', '=', $id)
            ->get();

        $consoles = DB::table('game_net_consoles')
            ->join('gem_net_prices', function ($join) {
                $join->on('gem_net_prices.console', '=', 'consoles.id');
            })
            ->where('game_net_consoles.game_net', '=', $id)
            ->get();

        $games = DB::table('game_net_games')
            ->join('games', function ($join) {
                $join->on('game_net_games.game', '=', 'games.id');
            })
            ->where('game_net_games.game_net', '=', $id)
            ->get();

        return view('', compact('games', 'game_net', 'consoles'));
    }


}
