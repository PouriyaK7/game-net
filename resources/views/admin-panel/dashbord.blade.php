@extends('layouts.first-layout.first-layout')

@section('content')
    <div class="table-responsive">
        <span class="login100-form-title p-b-41">
            Dashboard
        </span>
        <table class="table table-hover table-dark">
            <thead class="thead-inverse">
                <tr>
                    <th scope="col" colspan="7">
                        Hello {{ \Auth::user()->name }}
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <a href="">
                            Add game net
                        </a>
                    </td>
                    <td>
                        <a href="">
                            Add game
                        </a>
                    </td>
                    <td>
                        <a href="">
                            Add console
                        </a>
                    </td>
                    <td>
                        <a href="">
                            Add sth
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection
