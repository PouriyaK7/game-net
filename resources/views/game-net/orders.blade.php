@extends('layouts.first-layout.first-layout')
@section('content')
    <?php $request_btn = 1; ?>
    @if(isset($_GET['error']))
        <script>
            alert("Couldn't add order");
        </script>
    @endif
    <div class="table-responsive">
        <span class="login100-form-title p-b-35">
            Orders <a href="{{ URL::to('game-net/paid-orders') }}"><span style="font-size: 12px;color: #ccc"> __Go to paid orders</span></a>
        </span>
        <table style="white-space: nowrap" class="table table-hover table-dark">
            <thead class="thead-inverse">
            <tr>
                <th scope="col" style="padding: .95rem">
                    id
                </th>
                <th scope="col" style="padding: .95rem">
                    console
                </th>
                <th scope="col" style="padding: .95rem">
                    GP count
                </th>
                <th scope="col" style="padding: .95rem">
                    time
                </th>
                <th scope="col" style="padding: .95rem">
                    price
                </th>
                <th scope="col" style="padding: .95rem">
                    date
                </th>
                <th scope="col" style="padding: .95rem">
                    description
                </th>
                <th scope="col" style="padding: .95rem">
                    End game
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($x as $item)
                <tr>
                    <th scope="row">
                        {{ $item->order_id }}
                    </th>
                    <td>
                        {{ $item->code }}
                    </td>
                    <td>
                        {{ $item->gamepad_count }}
                    </td>
                    <td>
                        {{ $item->time }}
                    </td>
                    <td>
                        {{ $item->order_price }} T
                    </td>
                    <td>
                        {{ $item->order_date }}
                    </td>
                    <td>
                        {{ $item->order_description }}
                    </td>
                    <td>
                        <a href="{{ URL::to('game-net/finish-order/'. $item->order_id ) }}">
                            End game
                        </a>
                    </td>
                </tr>
            @endforeach
            <tr>
                <td colspan="8">
                    <a href="{{ URL::to('game-net/add-order') }}" style="display: block;text-align: center; font-weight: bold">
                        Add order
                    </a>
                </td>
            </tr>
            </tbody>
        </table>
        {{ $x->links() }}

    </div>
@endsection
