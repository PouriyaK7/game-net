@extends('layouts.first-layout.first-layout')

@section('content')
    <?php $request_btn = 1; ?>
    <div class="table-responsive">
        <span class="login100-form-title p-b-35">
            Paid Orders <br><span style="font-size: 20px;">Sum price: {{ $sum }} T</span>
        </span>
        <table class="table table-hover table-dark">
            <thead class="thead-inverse">
            <tr>
                <th style="padding: .95rem" scope="col">
                    id
                </th>
                <th style="padding: .95rem" scope="col">
                    console
                </th>
                <th style="padding: .95rem" scope="col">
                    GP count
                </th>
                <th style="padding: .95rem" scope="col">
                    time
                </th>
                <th style="padding: .95rem" scope="col">
                    price
                </th>
                <th style="padding: .95rem" scope="col">
                    date
                </th>
                <th style="padding: .95rem" scope="col">
                    description
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($x as $item)
                <tr>
                    <th scope="row">
                        {{ $item->order_id }}
                    </th>
                    <td>
                        {{ $item->code }}
                    </td>
                    <td>
                        {{ $item->gamepad_count }}
                    </td>
                    <td>
                        {{ $item->time }}
                    </td>
                    <td>
                        {{ $item->order_price }} T
                    </td>
                    <td>
                        {{ $item->order_date }}
                    </td>
                    <td>
                        {{ $item->order_description }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $x->links() }}

    </div>
@endsection
