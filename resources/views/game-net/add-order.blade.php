@extends('layouts.first-layout.first-layout')
@section('content')
    <style>

        .hi-select {
            font-family: Ubuntu-Regular;
            font-size: 20px;
            color: #555555;
            line-height: 1.2;
            display: block;
            width: 100%;
            height: 50px;
            background: transparent;
            padding: 0 10px 0 80px;
            -webkit-transition: all 0.4s;
            -o-transition: all 0.4s;
            -moz-transition: all 0.4s;
            transition: all 0.4s;
            border: none;
            outline: none;
        }

        .hi-select:focus {
            padding: 0 10px 0 60px;
        }
        .my-error-class {
            color: red;
            text-align: center;
            font-family: Ubuntu-Bold;
            font-size: 20px;
            line-height: 1.2;
            display: block;
            text-transform: uppercase;
            padding-bottom: 10px;
        }
    </style>
    <div class="wrap-login100 p-t-30 p-b-50">
            <span class="login100-form-title p-b-41">
                Add order
            </span>
        @if(isset($_GET['error']) && @$_GET['error'] == 'activation-is-passed')
            <span class="my-error-class">
                    Activation time passed
                </span>
        @endif
        <form method="post" action="{{ URL::to('game-net/operations/add-order') }}" class="login100-form validate-form p-b-33 p-t-5">

            @csrf

            <div class="wrap-input100 validate-input" data-validate = "Enter username">
                <select class="hi-select" name="console">
                    @foreach($x as $item)
                        <option value="{{ $item->id }}">
                            {{ $item->code  }}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="wrap-input100 validate-input" data-validate = "Enter username">
                <input class="input100" type="text" name="time" placeholder="Time..." required>
            </div>

            <div class="wrap-input100 validate-input" data-validate = "Enter username">
                <select class="hi-select" name="gamepad-count">
                    <option value="0">Gamepads count</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                </select>
            </div>

            <div class="wrap-input100 validate-input" data-validate = "Enter username">
                <input class="input100" type="text" name="description" placeholder="Description..." required>
            </div>

            <div class="container-login100-form-btn m-t-32">
                <button class="login100-form-btn" type="submit">
                    Login
                </button>
            </div>

        </form>
    </div>

@endsection
