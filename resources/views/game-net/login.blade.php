@extends('layouts.first-layout.first-layout')
@section('content')
    <style>
        .my-error-class {
            color: red;
            text-align: center;
            font-family: Ubuntu-Bold;
            font-size: 20px;
            line-height: 1.2;
            display: block;
            text-transform: uppercase;
            padding-bottom: 10px;
        }
    </style>
    <div class="wrap-login100 p-t-30 p-b-50">
            <span class="login100-form-title p-b-41">
                Account Login
            </span>
            @if(isset($_GET['error']) && @$_GET['error'] == 'activation-is-passed')
                <span class="my-error-class">
                    Activation time passed
                </span>
            @endif
        <form method="post" action="{{ URL::to('login') }}" class="login100-form validate-form p-b-33 p-t-5">

            @csrf

            <div class="wrap-input100 validate-input" data-validate = "Enter username">
                <input class="input100" type="email" name="email" placeholder="E-mail...">
                    <span class="focus-input100" data-placeholder="&#xe82a;"></span>
            </div>

            <div class="wrap-input100 validate-input" data-validate="Enter password">
                <input class="input100" type="password" name="password" placeholder="Password..." minlength="8">
                <span class="focus-input100" data-placeholder="&#xe80f;"></span>
            </div>

            <div class="container-login100-form-btn m-t-32">
                <button class="login100-form-btn" type="submit">
                    Login
                </button>
            </div>

        </form>
    </div>

@endsection
