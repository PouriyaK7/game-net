<!DOCTYPE html>
<html lang="en">
<head>
    <title>Game Net</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="{{ URL::to('login-page/images/icons/favicon.ico') }}"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::to('login-page/vendor/bootstrap/css/bootstrap.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::to('login-page/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::to('login-page/fonts/Linearicons-Free-v1.0.0/icon-font.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::to('login-page/vendor/animate/animate.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::to('login-page/vendor/css-hamburgers/hamburgers.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::to('login-page/vendor/animsition/css/animsition.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::to('login-page/vendor/select2/select2.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::to('login-page/vendor/daterangepicker/daterangepicker.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::to('login-page/css/util.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::to('login-page/css/main.css') }}">
    <!--===============================================================================================-->
    <style>
        .request-changes-btn {
            position: absolute;
            top: 20px;
            left: 20px;
            background-color: #fff;
            padding: 5px;
            border-radius: 20px;
            color: #000;
            transition: all .4s;
            cursor: pointer;
        }

        .request-changes-btn:hover {
            background-color: #eee;
        }
    </style>
</head>
<body>

<div class="limiter">
    <div class="container-login100" style="background-image: url({{ URL::to('login-page/images/bg-01.jpg') }});">
        @if(isset($request_btn))
            <span class="request-changes-btn" onclick="location.href = '{{ URL::to('game-net/change-settings') }}'">
                Request changes
            </span>
        @endif
        @yield('content')
    </div>
</div>

<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
<script src="{{ URL::to('login-page/vendor/jquery/jquery-3.2.1.min.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ URL::to('login-page/vendor/animsition/js/animsition.min.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ URL::to('login-page/vendor/bootstrap/js/popper.js') }}"></script>
<script src="{{ URL::to('login-page/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ URL::to('login-page/vendor/select2/select2.min.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ URL::to('login-page/vendor/daterangepicker/moment.min.js') }}"></script>
<script src="{{ URL::to('login-page/vendor/daterangepicker/daterangepicker.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ URL::to('login-page/vendor/countdowntime/countdowntime.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ URL::to('login-page/js/main.js') }}"></script>

</body>
</html>
