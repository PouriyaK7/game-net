<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('game_net')->nullable();
            $table->timestamp('start_date')->useCurrent()->nullable();
            $table->dateTime('end_date')->nullable();
            $table->integer('access')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('game_net');
            $table->dropColumn('start_date');
            $table->dropColumn('end_date');
            $table->integer('access');
        });
    }
}
