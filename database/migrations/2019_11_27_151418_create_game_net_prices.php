<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGameNetPrices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_net_prices', function (Blueprint $table) {
            $table->integer('game_net');
            $table->integer('console');
            $table->integer('price');
            $table->integer('gamepad_count');
            $table->primary(['game_net', 'console', 'gamepad_count']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_net_prices');
    }
}
