<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToGameNets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('game-nets', function (Blueprint $table) {
            $table->string('office_number');
            $table->string('phone_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('game-nets', function (Blueprint $table) {
            $table->dropColumn('office_number');
            $table->dropColumn('phone_number');
        });
    }
}
